package com.gmail.jakob7dev.messages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.MissingResourceException;

public final class MessagesFactory {

	private static final Collection<MessagesProvider> REGISTERED_PROVIDERS = new ArrayList<>(0);

	private MessagesFactory() {
	}

	public static Collection<MessagesProvider> getRegisteredProviders() {
		return Collections.unmodifiableCollection(REGISTERED_PROVIDERS);
	}

	public static void registerProvider(final MessagesProvider provider) {
		if (REGISTERED_PROVIDERS.contains(provider)) {
			throw new IllegalArgumentException("Provider is already registered");
		}

		REGISTERED_PROVIDERS.add(provider);
	}

	public static String getMessage(final Message message, final Object... vars) {
		if (message == null) {
			throw new IllegalArgumentException("Message cannot be null");
		}

		if (message.getProvider() == null) {
			throw new IllegalArgumentException("Provider in message cannot be null");
		}

		if (!REGISTERED_PROVIDERS.contains(message.getProvider())) {
			throw new IllegalArgumentException("Provider is not registered");
		}

		if (message.getProvider().getBundle() == null) {
			throw new IllegalArgumentException("Bundle in provider cannot be null");
		}

		if (message.getKey() == null) {
			throw new IllegalArgumentException("Key cannot be null");
		}

		String translation = ((message.getProvider().getDef() != null) && !message.getProvider().getDef().isEmpty()) ? message.getProvider().getDef() : (((message.getDef() != null) && !message.getDef().isEmpty()) ? message.getDef() : ("Message '" + message.getKey() + "' not found"));

		try {
			final String str = message.getProvider().getBundle().containsKey(message.getKey()) ? message.getProvider().getBundle().getString(message.getKey()) : null;

			if (str != null) {
				translation = (vars.length == 0) ? str : MessageFormat.format(str, vars);
			}
		} catch (final MissingResourceException ignored) {
		}

		return translation;
	}

}