package com.gmail.jakob7dev.messages;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public abstract class MessagesProvider {

	private final String name;
	private final File file;
	private final ResourceBundle bundle;
	private final String def;

	public MessagesProvider(final String name, final File file) {
		this(name, file, null);
	}

	public MessagesProvider(final String name, final File file, final String def) {
		this.name = name;
		this.file = file;
		this.bundle = this.loadBundle(this.file);
		this.def = def;
	}

	public String getName() {
		return this.name;
	}

	public File getFile() {
		return this.file;
	}

	public ResourceBundle getBundle() {
		return this.bundle;
	}

	public String getDef() {
		return this.def;
	}

	public ResourceBundle loadBundle(final File file) {
		try {
			return ResourceBundle.getBundle(this.name);
		} catch (final Exception ex) {
			if (file == null) {
				throw new IllegalArgumentException("File cannot be null");
			}

			if (!file.isFile()) {
				throw new IllegalArgumentException("File must be file");
			}

			if (!file.exists()) {
				throw new IllegalArgumentException("File is not exists");
			}

			try {
				try (FileReader reader = new FileReader(file)) {
					return new PropertyResourceBundle(reader);
				}
			} catch (final IOException ex1) {
				throw new RuntimeException("Could not load bundle for provider", ex1);
			}
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj == null) || (this.getClass() != obj.getClass())) {
			return false;
		}

		final MessagesProvider that = (MessagesProvider) obj;

		return (this.name != null) ? this.name.equals(that.name) : ((that.name == null) && ((this.file != null) ? this.file.equals(that.file) : ((that.file == null) && ((this.bundle != null) ? this.bundle.equals(that.bundle) : ((that.bundle == null) && ((this.def != null) ? this.def.equals(that.def) : (that.def == null)))))));
	}

	@Override
	public int hashCode() {
		int result = (this.name != null) ? this.name.hashCode() : 0;

		result = (31 * result) + ((this.file != null) ? this.file.hashCode() : 0);
		result = (31 * result) + ((this.bundle != null) ? this.bundle.hashCode() : 0);
		result = (31 * result) + ((this.def != null) ? this.def.hashCode() : 0);

		return result;
	}

	@Override
	public String toString() {
		return "MessagesProvider{" + "name='" + this.name + '\'' + ", file=" + this.file + ", bundle=" + this.bundle + ", def='" + this.def + '\'' + '}';
	}

}