package com.gmail.jakob7dev.messages;

public class Message {

	private final MessagesProvider provider;
	private final String key;
	private final String def;

	public Message(final Class<? extends MessagesProvider> provider, final String key) {
		this(provider, key, null);
	}

	public Message(final Class<? extends MessagesProvider> provider, final String key, final String def) {
		try {
			this.provider = provider.newInstance();
		} catch (final Exception ex) {
			throw new RuntimeException("Could not get new provider instance", ex);
		}

		this.key = key;
		this.def = def;
	}

	public MessagesProvider getProvider() {
		return this.provider;
	}

	public String getKey() {
		return this.key;
	}

	public String getDef() {
		return this.def;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj == null) || (this.getClass() != obj.getClass())) {
			return false;
		}

		final Message that = (Message) obj;

		return (this.provider != null) ? this.provider.equals(that.provider) : ((that.provider == null) && ((this.key != null) ? this.key.equals(that.key) : ((that.key == null) && ((this.def != null) ? this.def.equals(that.def) : (that.def == null)))));
	}

	@Override
	public int hashCode() {
		int result = (this.provider != null) ? this.provider.hashCode() : 0;

		result = (31 * result) + ((this.key != null) ? this.key.hashCode() : 0);
		result = (31 * result) + ((this.def != null) ? this.def.hashCode() : 0);

		return result;
	}

	@Override
	public String toString() {
		return "Message{" + "provider=" + this.provider + ", key='" + this.key + '\'' + ", def='" + this.def + '\'' + '}';
	}

}